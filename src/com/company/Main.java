package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static void decision(long a) {
        double b = a;
        int i = 0;
        while ((b*b > a) && (i < 200)) {
            b = (b + a/b) /2;
            i++;
        }
        System.out.println(b);
    }

    public static void main(String[] args) throws IOException {
        if (Arrays.stream(args).count() == 0) {
            System.out.println("Please enter input data");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            long data = Long.parseLong(reader.readLine());
            decision(data);
            return;
        }
    }
}